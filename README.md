### Coinbase price monitor

Current project version : 1.0.0

Ce projet permet de récupérer et d'afficher le prix d'une paire asset/asset depuis Coinbase

## Pré-requis

Un compte Coinbase et Coinbase Pro ( précédemment GDAX )
Version de node : minimum `v9.0.0`

## Instalation

Cloner le projet

Lancer un `npm install`

Remplir les paramètres de configuration dans le fichier `config.dist.js` et renommer le fichier en `config.js`

Pour la génération d'une clé API Coinbase, voir ce lien : https://support.coinbase.com/customer/en/portal/articles/1914910-how-can-i-generate-api-keys-within-coinbase-commerce-

Le paramètre `apiVersion` prend la dernière version présente ici : https://developers.coinbase.com/api/v2#changelog 

## Utilisation

Le service se lance avec la commande `node server.js`
Par défault, le serveur va se lancer avec la paire `BTC-EUR`, et le prix initial d'achat contenu dans la config

Pour spécifier une paire et un prix de départ, lancer `node server.js <TRADING-PAIR> <INITIAL-PRICE>`

TRADING-PAIR : Une chaine de caractères du type `"PAIRE-PAIRE"`

INITIAL-PRICE : Un prix d'achat initial auquel comparer le prix actuel du type `XX.XX`

## Exemple 

Exemple de la commande `node servers.js LTC-EUR 66.72`

![Alt text](example.png?raw=true "Exemple")
