const config = require('./config')
const functions = require('./functions')

const PRIVATE_KEY = config.PRIVATE_KEY
const PUBLIC_KEY = config.PUBLIC_KEY
const baseURL = config.apiBaseUrl
const apiVersion = config.apiVersion
const lastBuyPrice = config.lastBuyPrice

const Client = require('coinbase').Client;
const client = new Client({
  'apiKey': PUBLIC_KEY, 
  'apiSecret': PRIVATE_KEY, 
  'version': apiVersion
});

getTradingPair = function() {
  if (process.argv[2]) {
    return process.argv[2].toString()
  }
 return "BTC-EUR"
}

getStartingPrice = function() {
  if (process.argv[3]) {
    return process.argv[3]
  }
  // Default should return the current market price
  // currently returns the value entered in config
  return lastBuyPrice
}

getSpotPrice = function() {
  client.getSpotPrice({'currencyPair': getTradingPair()}, function(err, price) {
    const retreivedPrice = price.data.amount
    const lastPrice = parseFloat(getStartingPrice())
    if (retreivedPrice > lastPrice) {
      console.log("\x1b[32m%s\x1b[0m", retreivedPrice, 
        " +", functions.round(parseFloat(retreivedPrice - lastPrice), 4)
      );  
    } else if (retreivedPrice < lastPrice) {
      console.log('\x1b[31m%s\x1b[0m', retreivedPrice, 
        functions.round(parseFloat(retreivedPrice - lastPrice), 4)
      );
    } else {
      console.log('\x1b[33m%s\x1b[0m', retreivedPrice);
    }
  });
}
console.log('-------------')
console.log('PAIRE : ', getTradingPair())
console.log('Start price : ', getStartingPrice())
console.log('-------------')
setInterval(function() { getSpotPrice() }, 6000)